﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlBaby : MonoBehaviour
{
    public bool isJ1;

    public bool isGoalkeeper;
    public bool isDefender;
    public bool isMidfielder;
    public bool isStriker;

    public bool[] isPs;

    public float angleMin = 20;
    public float angleMax = 160;

    public Quaternion initRot;
    public float footSpeed = -4;
    public float footSpeedBack = 10;
    public float speedMove = -4;

    public float posMinGoal = -0.9f;
    public float posMaxGoal = 0.9f;
    public float posMinDefender = -1.8f;
    public float posMaxDefender = 1.8f;
    public float posMinMidfielder = -0.55f;
    public float posMaxMidfielder = 0.55f;
    public float posMinStriker = -1.23f;
    public float posMaxStriker = 1.23f;

    public Toggle[] toggles;

    // Start is called before the first frame update
    void Start()
    {
        initRot = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // JOUEUR 1 

        if (isJ1)
        {
            if (toggles[0].isOn)
            {
                isPs[0] = true;
            }
            else
            {
                isPs[0] = false;
            }

            // CONTROLES PLAYSTATION

            if (isPs[0])
            {
                float FootDirection = Mathf.Clamp(-Input.GetAxis("UnshootPs") + Input.GetAxis("ShootPs"), -1, 1);

                float MoveSD = Input.GetAxis("RightJoystickVerticalPS");
                float MoveMG = Input.GetAxis("LeftJoystickVerticalPS");

                if (FootDirection != 0)
                {
                    transform.Rotate(0, FootDirection * footSpeed * Time.deltaTime, 0);
                }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, initRot, footSpeedBack * Time.deltaTime);
                    //GetComponentInChildren<Transform>().rotation = new Quaternion(-90, 0, 0, 0);
                }

                //float clampedValue = Mathf.Clamp(transform.eulerAngles.x, angleMin, angleMax);

                //transform.eulerAngles = new Vector3(clampedValue, transform.eulerAngles.y, transform.eulerAngles.z);

                if (isGoalkeeper)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveMG * speedMove * Time.deltaTime), posMinGoal, posMaxGoal);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isDefender)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveSD * speedMove * Time.deltaTime), posMinDefender, posMaxDefender);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isMidfielder)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveMG * speedMove * Time.deltaTime), posMinMidfielder, posMaxMidfielder);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isStriker)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveSD * speedMove * Time.deltaTime), posMinStriker, posMaxStriker);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
            }

            // CONTROLES XBOX

            if (isPs[0] == false)
            {
                float FootDirection = Mathf.Clamp(-Input.GetAxis("UnshootXbox") + Input.GetAxis("ShootXbox"), -1, 1);
                
                float MoveSD = Input.GetAxis("RightJoystickVerticalXbox");
                float MoveMG = Input.GetAxis("LeftJoystickVerticalXbox");

                if (FootDirection != 0)
                {
                    transform.Rotate(0, FootDirection * footSpeed * Time.deltaTime, 0);
                }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, initRot, footSpeedBack * Time.deltaTime);
                }

                //float clampedValue = Mathf.Clamp(transform.eulerAngles.z, angleMin, angleMax);

                //transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, clampedValue);

                if (isGoalkeeper)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveMG * speedMove * Time.deltaTime), posMinGoal, posMaxGoal);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isDefender)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveSD * speedMove * Time.deltaTime), posMinDefender, posMaxDefender);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isMidfielder)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveMG * speedMove * Time.deltaTime), posMinMidfielder, posMaxMidfielder);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isStriker)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveSD * speedMove * Time.deltaTime), posMinStriker, posMaxStriker);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
            }
        }

        // JOUEUR 2

        if (isJ1 == false)
        {
            if (toggles[1].isOn)
            {
                isPs[1] = true;
            }
            else
            {
                isPs[1] = false;
            }

            // CONTROLES PLAYSTATION

            if (isPs[1])
            {
                float FootDirection = Mathf.Clamp(-Input.GetAxis("P2UnshootPs") + Input.GetAxis("P2ShootPs"), -1, 1);

                float MoveSD = Input.GetAxis("P2LeftJoystickVerticalPS");
                float MoveMG = Input.GetAxis("P2RightJoystickVerticalPS");

                //float testValue = 90;
                if (FootDirection != 0)
                {
                    transform.Rotate(0, FootDirection * footSpeed * Time.deltaTime, 0);

                    /*if (FootDirection > 0)
                    {
                        if(FootDirection == 2 && testValue < 80)
                        {
                            Debug.Log("cc");
                            transform.rotation = Quaternion.Lerp(transform.rotation, new Quaternion(20,-90,-90,0), 1);
                        }
                        else
                        {
                            testValue = testValue + (5 * footSpeed * Time.deltaTime);
                        }
                        
                    }
                    if (FootDirection > 0)
                    {
                        testValue = testValue + (5 * footSpeed * Time.deltaTime);
                    }

                    //Debug.Log(testValue);*/
                }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, initRot, footSpeedBack * Time.deltaTime);
                }

                //float clampedValue = Mathf.Clamp(transform.eulerAngles.x, angleMin, angleMax);
                //float clampedValue = Mathf.Clamp(testValue, angleMin, angleMax);

                //transform.eulerAngles = new Vector3(clampedValue, transform.eulerAngles.y, transform.eulerAngles.z);
                //transform.eulerAngles = new Vector3(clampedValue, -90, -90);

                if (isGoalkeeper)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveMG * speedMove * Time.deltaTime), posMinGoal, posMaxGoal);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isDefender)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveSD * speedMove * Time.deltaTime), posMinDefender, posMaxDefender);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isMidfielder)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveMG * speedMove * Time.deltaTime), posMinMidfielder, posMaxMidfielder);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isStriker)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveSD * speedMove * Time.deltaTime), posMinStriker, posMaxStriker);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
            }

            // CONTROLES XBOX

            if (isPs[1] == false)
            {
                float FootDirection = Mathf.Clamp(-Input.GetAxis("P2UnshootXbox") + Input.GetAxis("P2ShootXbox"), -1, 1);

                float MoveSD = Input.GetAxis("P2LeftJoystickVerticalXbox");
                float MoveMG = Input.GetAxis("P2RightJoystickVerticalXbox");

                if (FootDirection != 0)
                {
                    transform.Rotate(0, FootDirection * footSpeed * Time.deltaTime, 0);
                    //GetComponentInChildren<Transform>().rotation = new Quaternion(-90, 0, 0, 0);
                }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, initRot, footSpeedBack * Time.deltaTime);
                }

                //float clampedValue = Mathf.Clamp(transform.eulerAngles.x, angleMin, angleMax);

                //transform.eulerAngles = new Vector3(clampedValue, transform.eulerAngles.y, transform.eulerAngles.z);

                if (isGoalkeeper)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveMG * speedMove * Time.deltaTime), posMinGoal, posMaxGoal);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isDefender)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveSD * speedMove * Time.deltaTime), posMinDefender, posMaxDefender);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isMidfielder)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveMG * speedMove * Time.deltaTime), posMinMidfielder, posMaxMidfielder);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isStriker)
                {
                    float clampedZ = Mathf.Clamp(transform.position.z + (MoveSD * speedMove * Time.deltaTime), posMinStriker, posMaxStriker);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
            }
        }
    }
}

