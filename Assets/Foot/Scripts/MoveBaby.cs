﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBaby : MonoBehaviour
{
    public bool isJ1;

    public bool isGoalkeeper;
    public bool isDefender;
    public bool isMidfielder;
    public bool isStriker;

    public bool[] isPs;

    public float angleMin = 20;
    public float angleMax = 160;

    public Quaternion initRot;
    public float speed = -4;
    public float speed2 = -4;

    // Start is called before the first frame update
    void Start()
    {
        initRot = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // JOUEUR 1 

        if (isJ1)
        {
            // CONTROLES PLAYSTATION

            if (isPs[0])
            {
                Vector3 inputDirection = Vector3.zero;
                inputDirection.x = -Input.GetAxis("P2UnshootPs") + Input.GetAxis("P2ShootPs");
                Vector3 inputDirectionSD = Vector3.zero;
                inputDirectionSD.z = Input.GetAxis("P2LeftJoystickVerticalPS");
                Vector3 inputDirectionMG = Vector3.zero;
                inputDirectionMG.z = Input.GetAxis("P2RightJoystickVerticalPS");

                if (inputDirection.x != 0)
                {
                    transform.Rotate(0, inputDirection.x * speed * Time.deltaTime, 0);

                }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, initRot, 10 * Time.deltaTime);
                }

                float clampedValue = Mathf.Clamp(transform.eulerAngles.x, angleMin, angleMax);

                transform.eulerAngles = new Vector3(clampedValue, transform.eulerAngles.y, transform.eulerAngles.z);

                if (isGoalkeeper)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-0.9f, 0.9f, (inputDirection2.z + 1) / 2));
                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionMG.z * speed2 * Time.deltaTime), -0.9f, 0.9f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isDefender)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-1.8f, 1.8f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionSD.z * speed2 * Time.deltaTime), -1.8f, 1.8f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isMidfielder)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-0.33f, 0.33f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionMG.z * speed2 * Time.deltaTime), -0.55f, 0.55f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isStriker)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-1.23f, 1.23f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionSD.z * speed2 * Time.deltaTime), -1.23f, 1.23f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
            }

            if (isPs[0] == false)
            {
                Vector3 inputDirection = Vector3.zero;
                inputDirection.x = -Input.GetAxis("P2UnshootXbox") + Input.GetAxis("P2ShootXbox");
                Vector3 inputDirectionSD = Vector3.zero;
                inputDirectionSD.z = Input.GetAxis("P2LeftJoystickVerticalXbox");
                Vector3 inputDirectionMG = Vector3.zero;
                inputDirectionMG.z = Input.GetAxis("P2RightJoystickVerticalXbox");

                if (inputDirection.x != 0)
                {
                    transform.Rotate(0, inputDirection.x * speed * Time.deltaTime, 0);

                }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, initRot, 10 * Time.deltaTime);
                }

                float clampedValue = Mathf.Clamp(transform.eulerAngles.x, angleMin, angleMax);

                transform.eulerAngles = new Vector3(clampedValue, transform.eulerAngles.y, transform.eulerAngles.z);

                if (isGoalkeeper)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-0.9f, 0.9f, (inputDirection2.z + 1) / 2));
                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionMG.z * speed2 * Time.deltaTime), -0.9f, 0.9f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isDefender)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-1.8f, 1.8f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionSD.z * speed2 * Time.deltaTime), -1.8f, 1.8f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isMidfielder)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-0.33f, 0.33f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionMG.z * speed2 * Time.deltaTime), -0.55f, 0.55f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isStriker)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-1.23f, 1.23f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionSD.z * speed2 * Time.deltaTime), -1.23f, 1.23f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
            }
        }

        //inputDirection.z = Input.GetAxis("LeftJoystickVertical");
        //transform.position = transform.position + inputDirection;

        if (isJ1 == false)
        {
            if (isPs[0])
            {
                
                Vector3 inputDirection2 = Vector3.zero;
                inputDirection2.x = -Input.GetAxis("UnshootPs") + Input.GetAxis("ShootPs");
                Vector3 inputDirectionSD = Vector3.zero;
                inputDirectionSD.z = Input.GetAxis("RightJoystickVerticalPS");
                Vector3 inputDirectionMG = Vector3.zero;
                inputDirectionMG.z = Input.GetAxis("LeftJoystickVerticalPS");

                if (inputDirection2.x != 0)
                {
                    transform.Rotate(0, inputDirection2.x * speed * Time.deltaTime, 0);
                    
                }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, initRot, 10 * Time.deltaTime);
                }

                float clampedValue = Mathf.Clamp(transform.eulerAngles.x, angleMin, angleMax);

                transform.eulerAngles = new Vector3(clampedValue, transform.eulerAngles.y, transform.eulerAngles.z);



                if (isGoalkeeper)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-0.9f, 0.9f, (inputDirection2.z + 1) / 2));
                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionMG.z * speed2 * Time.deltaTime), -0.9f, 0.9f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isDefender)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-1.8f, 1.8f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionSD.z * speed2 * Time.deltaTime), -1.8f, 1.8f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isMidfielder)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-0.33f, 0.33f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionMG.z * speed2 * Time.deltaTime), -0.55f, 0.55f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isStriker)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-1.23f, 1.23f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionSD.z * speed2 * Time.deltaTime), -1.23f, 1.23f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
            }

            if (isPs[0] == false)
            {
                Vector3 inputDirection2 = Vector3.zero;
                inputDirection2.x = -Input.GetAxis("UnshootXbox") + Input.GetAxis("ShootXbox");
                Vector3 inputDirectionSD = Vector3.zero;
                inputDirectionSD.z = Input.GetAxis("RightJoystickVerticalXbox");
                Vector3 inputDirectionMG = Vector3.zero;
                inputDirectionMG.z = Input.GetAxis("LeftJoystickVerticalXbox");

                if (inputDirection2.x != 0)
                {
                    transform.Rotate(0, inputDirection2.x * speed * Time.deltaTime, 0);

                }
                else
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, initRot, 10 * Time.deltaTime);
                }

                float clampedValue = Mathf.Clamp(transform.eulerAngles.x, angleMin, angleMax);

                transform.eulerAngles = new Vector3(clampedValue, transform.eulerAngles.y, transform.eulerAngles.z);

                if (isGoalkeeper)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-0.9f, 0.9f, (inputDirection2.z + 1) / 2));
                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionMG.z * speed2 * Time.deltaTime), -0.9f, 0.9f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isDefender)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-1.8f, 1.8f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionSD.z * speed2 * Time.deltaTime), -1.8f, 1.8f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isMidfielder)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-0.33f, 0.33f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionMG.z * speed2 * Time.deltaTime), -0.55f, 0.55f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
                if (isStriker)
                {
                    //transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(-1.23f, 1.23f, (inputDirection2.z + 1) / 2));

                    float clampedZ = Mathf.Clamp(transform.position.z + (inputDirectionSD.z * speed2 * Time.deltaTime), -1.23f, 1.23f);

                    transform.position = new Vector3(transform.position.x, transform.position.y, clampedZ);
                }
            }
        }         
    }
}
