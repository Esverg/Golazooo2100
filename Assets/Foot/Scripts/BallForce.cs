﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallForce : MonoBehaviour
{
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player" || collision.gameObject.tag == "Player2")
        {
            rb.AddForce(new Vector3(transform.position.x - collision.transform.position.x, transform.position.y - collision.transform.position.y, transform.position.z - collision.transform.position.z) * 500);
        }
    }
}
