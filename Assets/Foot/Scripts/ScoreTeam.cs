﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTeam : MonoBehaviour
{

    public int score = 0;
    public Text scoreTxt;
    public GameObject explosion;

    private void Start()
    {
        explosion.SetActive(false);
    }
    void Update()
    {
        scoreTxt.text = score.ToString() +"/10";
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Ballon")
        {
            score++;
            UpdateExplosion();
        }
    }

    private void UpdateExplosion()
    {
        explosion.SetActive(false);
        explosion.SetActive(true);
    }

}
