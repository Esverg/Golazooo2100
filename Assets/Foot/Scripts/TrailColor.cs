﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailColor : MonoBehaviour
{

    private TrailRenderer myTrailRenderer;
    Gradient redGradient = new Gradient();
    Gradient blueGradient = new Gradient();

    void Start()
    {
        myTrailRenderer = GetComponent<TrailRenderer>();

        redGradient.SetKeys(
new GradientColorKey[] { new GradientColorKey(Color.red, 0.0f), new GradientColorKey(Color.white, 1.0f) },
new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(0f, 1.0f) });

        blueGradient.SetKeys(
new GradientColorKey[] { new GradientColorKey(Color.blue, 0.0f), new GradientColorKey(Color.white, 1.0f) },
new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(0f, 1.0f) });
    }

    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            myTrailRenderer.colorGradient = redGradient;
        }

        if (collision.gameObject.tag == "Player2")
        {
            myTrailRenderer.colorGradient = blueGradient;
        }
    }
}
