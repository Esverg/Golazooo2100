﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gamelle : MonoBehaviour
{
    public float scoreJ1 = 0;
    public float scoreJ2 = 0;

    public Text[] txtScore;
    public Text txtCaRebours;

    public Image[] Uicontrols;

    public bool InGoal1 = false;
    public bool InGoal2 = false;

    public GameObject[] explosions;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < explosions.Length; i++)
        {
            explosions[i].SetActive(false);
        }

        txtCaRebours.gameObject.SetActive(false);
        txtScore[0].text = scoreJ1.ToString() + "/10";
        txtScore[1].text = scoreJ2.ToString() + "/10";

        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().useGravity = false;
        StartCoroutine(StartGame());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator StartGame()
    {
        yield return new WaitForSeconds(2);
        txtCaRebours.gameObject.SetActive(true);
        txtCaRebours.text = "3";
        yield return new WaitForSeconds(1);
        txtCaRebours.text = "2";
        yield return new WaitForSeconds(1);
        txtCaRebours.text = "1";
        for (int i = 0; i < Uicontrols.Length; i++)
        {
            Uicontrols[i].gameObject.SetActive(false);
        }
        yield return new WaitForSeconds(1);
        txtCaRebours.gameObject.SetActive(false);
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().velocity = new Vector3(Random.Range(0, 2) * 2 - 1, 0, 0);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ButJ1")
        {
            scoreJ2++;
            UpdateExplosionJ2();
            txtScore[1].text = scoreJ2.ToString() + "/10";
        }

        if (other.gameObject.name == "ButJ2")
        {
            scoreJ1++;
            UpdateExplosionJ1();
            txtScore[0].text = scoreJ1.ToString() + "/10";
        }

        if (other.gameObject.name == "DetectorGamelleBut1")
        {
            InGoal1 = true;
        }
        if (other.gameObject.name == "DetectorGamelleBut2")
        {
            InGoal2 = true;
        }

        if(InGoal1 && other.gameObject.name == "DetectorInBaby")
        {
            scoreJ1--;
            txtScore[0].text = scoreJ1.ToString() + "/10";
        }
        if (InGoal2 && other.gameObject.name == "DetectorInBaby")
        {
            scoreJ2--;
            txtScore[1].text = scoreJ2.ToString() + "/10";
        }

        if(other.gameObject.name == "RespawnBall")
        {
            InGoal1 = false;
            InGoal2 = false;

            GetComponent<Rigidbody>().velocity = Vector3.zero;
            transform.position = new Vector3(0, 3, 0);
            GetComponent<Rigidbody>().velocity = new Vector3(Random.Range(0, 2) * 2 - 1, 0, 0);
        }
    }

    public void UpdateExplosionJ1()
    {
        explosions[0].SetActive(false);
        explosions[0].SetActive(true);
    }

    public void UpdateExplosionJ2()
    {
        explosions[1].SetActive(false);
        explosions[1].SetActive(true);
    }
}
