﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    public void OnClickPlay()
    {
        SceneManager.LoadScene(1);
    }

    public void OnClickMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void OnClickQuit()
    {
        Application.Quit();
        
    }

    public void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            if (FindObjectOfType<Gamelle>().scoreJ1 >= 10)
            {
                SceneManager.LoadScene(2);
            }

            if (FindObjectOfType<Gamelle>().scoreJ2 >= 10)
            {
                SceneManager.LoadScene(3);
            }
        }
    }
}
